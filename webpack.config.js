const path = require('path')
const webpack = require('webpack');
const htmlWebpackPugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');//css 优化
const envConfig = require('./config/env');
let devServer = {
    host: 'localhost',
    port: 3003,
    open: true,
    hot: true,
    compress: true,
    historyApiFallback: true,
    contentBase:'./dist',
    progress: true, // 进度条
    proxy: {
        '/api': {
            target: 'http://localhost:3003',
            secure: false,
            changeOrigin: true,
            pathRewrite: {
                '^/api': ''
            }
        }
    },
}
module.exports = {
    mode: process.env.NODE_ENV == 'dev'? "development" :"production",
    entry: [
        "@babel/polyfill", path.resolve(__dirname, './src/main.js')
    ],
    output: {
        path:path.resolve(__dirname,'dist'),
        filename:'js/[name].[hash:8].js'
    },
    resolve: {
        extensions: [".js", ".jsx"], //引入文件自动识别为js或者jsx
    },
    module: {
        rules: [
            {
                test: /\.(css)$/,
                use: ['style-loader', {
                    loader: 'css-loader',
                    options: {
                      importLoaders: 1,
                    },
                  }, 'postcss-loader'],
              },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    // process.env.NODE_ENV == 'dev' ? 'style-loader': MiniCssExtractPlugin.loader,//热更新
                    "css-loader",
                    "sass-loader",
                ]
            },
            {
                test: /\.(js|jsx)$/, //添加支持jsx
                use: {
                  loader: "babel-loader", 
                },
            },
            {
                test: /\.(jpe?g|ypng|gif)$/i,
                exclude: [path.resolve(__dirname, './node_modules')],
                type: "asset",
                parser: {
                    dataUrlCondition: {
                        maxSize: 6 * 1024, // 6kb  指定大小
                    },
                },
                generator: {
                    filename:"img/[name].[hash:6][ext]"
                }
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                exclude: [path.resolve(__dirname, './node_modules')],
                type: "asset",
                generator: {
                    filename:"video/[name].[hash:6].[ext]"
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
                exclude: [path.resolve(__dirname, './node_modules')],
                type: "asset",
                generator: {
                    filename:"fonts/[name].[hash:6].[ext]"
                }
            },
        ]
    },
    optimization: {
        minimizer: [
            new CssMinimizerPlugin()//生产优化
        ]
        //minimize:true,//开启开发优化
    },
    plugins: [
        new webpack.DefinePlugin({
            envConfig: JSON.stringify(envConfig)
        }),
        new htmlWebpackPugin({
            template: path.resolve(__dirname,'./src/index.html')
        }),
        new CleanWebpackPlugin(),
        process.env.NODE_ENV == 'dev' ? new webpack.HotModuleReplacementPlugin():'' ,//热更新
        require('postcss-preset-env'),
        new MiniCssExtractPlugin({
            filename: 'css/[name].[hash:8].css',
            chunkFilename: '[id].css',
        }),
    ],
    devServer:process.env.NODE_ENV == 'dev' ? devServer :'',
}