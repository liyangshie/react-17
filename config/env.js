const env = process.env.NODE_ENV;
const config = {
    dev: {
        api: 'localhost'
    },
    pro: {
        api: '生产'
    },
    test: {
        api: '测试'
    },
    'pro-prd': {
        api: '预生产'
    },
};
module.exports = config[env];