import React from "react"
import { Menu ,Button} from 'antd'
import 'antd/dist/antd.css';
import './home.scss'
import { Link  ,Switch, BrowserRouter as Router, Route,Redirect} from "react-router-dom";
import { MailOutlined, AppstoreOutlined,SettingOutlined, MenuUnfoldOutlined, MenuFoldOutlined,} from '@ant-design/icons';
import Nopage from '../../components/Nopage'
const { SubMenu } = Menu;
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.props.history.push('/home/child')
    }
    state = {
        collapsed: false,
        width: 256,
        menuList: [
            {
                list: [
                    {
                        val: '数据1',
                        path: '/home/child',
                        query: 'query1'
                    },
                    {
                        val: '数据2',
                        path: '/home/child2',
                        query: 'query2'
                    },
                    {
                        val: '数据3',
                        path: '/home/index3',
                        query: 'query3'
                    },
                    {
                        val: '数据3',
                        path: '/home/index33',
                        query: 'query33'
                    },
                    {
                        val: '数据3',
                        path: '/home/index34',
                        query: 'query34'
                    },{
                        val: '数据3',
                        path: '/home/index35',
                        query: 'query35'
                    }
                    
                ],
                ico: '<MailOutlined/>',
                title:'title'
            },
            {
                list: [
                    {
                        val: '数据4',
                        path: '/home/index4',
                        query: 'query4'
                    },
                    {
                        val: '数据5',
                        path: '/home/index5',
                        query: 'query5'
                    },
                    {
                        val: '数据6',
                        path: '/home/index6',
                        query: 'query6'
                    }
                ],
                ico: '<AppstoreOutlined/>',
                title:'title'
            },
            {
                list: [
                    {
                        val: '数据7',
                        path: '/home/index7',
                        query: 'query7'
                    },
                    {
                        val: '数据8',
                        path: '/home/index8',
                        query: 'query8'
                    },
                    {
                        val: '数据9',
                        path: '/home/index9',
                        query: 'query9'
                    } 
                ],
                ico: '<SettingOutlined/>',
                title:'title'
            },
            {
                list: [
                    {
                        val: '数据10',
                        path: '/home/index10',
                        query: 'query10'
                    },
                    {
                        val: '数据11',
                        path: '/home/index11',
                        query: 'query11'
                    },
                    {
                        val: '数据12',
                        path: '/home/index12',
                        query: 'query12'
                    } 
                ],
                ico: '<SettingOutlined/>',
                title:'title'
            }
        ]
    };
    toggleCollapsed = () => {
        this.setState({
            collapsed: !this.state.collapsed,
            width: this.state.collapsed?256:100,
        });
    };
    render() {
        console.log("渲染次数")
        return (
            
            <div className="homecontainer">
                <div style={{ width: 256, height: '100%' }}>
                    <div className="hometitle" style={{ textAlign: "center", height: 60, lineHeight: '60px', color: '#fff' }}>
                        <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
                        <div className="ishometitle">
                            <Button  type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
                                {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                            </Button>
                        </div>
                    </div>
                    <Menu
                        onClick={this.handleClick}
                        style={{ width: this.state.width,height:'100%' }}
                        defaultOpenKeys={['sub1']}
                        selectedKeys={[this.state.current]}
                        mode="inline"
                        inlineCollapsed={this.state.collapsed}
                        theme="dark"
                        onSelect={ this.onMenuSelect}
                    >
                        {
                            this.state.menuList.map((item, key) => {
                                let ico;
                                if (key == 0) {
                                    ico = < MailOutlined />;
                                } else if (key == 1) {
                                    ico = < AppstoreOutlined />;
                                }else if (key == 2) {
                                    ico = < SettingOutlined />;
                                }else if (key == 3) {
                                    ico = < SettingOutlined />;
                                }
                                return(
                                    <SubMenu key={key} icon={ico} title={item.title}>
                                        {
                                            item.list.map((r, i) => {
                                                return (
                                                    <Menu.Item key={r.path} ><Link  to={r.path}>{r.val}</Link></Menu.Item>
                                                )
                                            })
                                        }
                                    </SubMenu>
                                )
                            })
                                
                        }
                    </Menu>
                </div>
                <div className='content'>
                    <Switch >
                        {
                            //console.log( this.props.routes), 获取当前路由下的子路由
                            this.props.routes.map((route, key) => {
                                return(
                                    <Route key={key} path={route.path}  component={route.component} ></Route>
                                )
                            })
                        }
                        <Route  component={ Nopage }></Route>
                    </Switch> 
                </div>
            </div>
        );
    }
}

export default Home