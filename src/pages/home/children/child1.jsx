import React from "react"
import { Link } from "react-router-dom"
import store from "../../../redux"
import { Button } from "antd"
import onAction  from "../../../redux/actions"
class As extends React.Component {
    constructor(props) { 
        super(props);
        this.state = store.getState();
        this.handleStoreChange = this.handleStoreChange.bind(this);
        this.onStorePromise = this.onStorePromise.bind(this);
        this.getLogin = this.getLogin.bind(this);
        store.subscribe(this.handleStoreChange);
    }
    loginRequest() {
        return {type:'JIAZAI_ZHONG'}
    }
    
    loginError() {
        return {type:'ERROR'}
    }
    
    loginResule(res) {
        return {type:'RESULT',payload:res}
    }

    //模拟数据响应
    getLogin() {
        return dispatch => {
            store.dispatch(this.loginRequest());
            setTimeout(() => {
                if (!true) {
                    return store.dispatch(this.loginResule('成功对象'))
                } else {
                    return store.dispatch(this.loginError())
                }
            },1000)
        }
    }

    onStorePromise() {
        store.dispatch(this.getLogin())
    }

    onStoreClick() {
        store.dispatch(onAction({ type: 'LOGIN_CESHI', payload: '修改' }))
    }

    onStoreClicks() {
        store.dispatch(onAction({ type: 'LOGIN_CESHI', payload: '返回修改' }))
    }

    handleStoreChange() {
        this.setState(store.getState())
    }

    render() {
        return (
            <div>
                <h1><Link to="/home/dd/details">详情1</Link></h1>
                <h1><Link to="/home/dd/details2">详情2</Link></h1>
                <h1><Link to="/home/dd/details3">详情3</Link></h1>
                <h1><Link to="/home/dd/details4">详情4</Link></h1>
                <h2>{this.state.inputValue}</h2>
                <h2>{ this.state.val}</h2>
                <Button onClick={this.onStoreClick}>修改store</Button>
                <Button onClick={this.onStoreClicks}>修改store</Button>
                <Button onClick={this.onStorePromise}>异步store</Button>
            </div>
            
        )
    }
}
export default As