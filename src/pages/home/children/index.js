import child1 from './child1'
import child2 from './child2'
import details from './details'
const routeChild = [
    {
        path: '/home/child',
        component: child1,
    },
    {
        path: '/home/child2',
        component: child2,
    },
    {
        path: '/home/dd/:id',
        component: details,
        // exact:true
    },
    {
        path: '/home/dd/:id',
        component: details,
        // exact:true
    }
]
export default routeChild