import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect 
} from "react-router-dom";

import Login from './login'
import Home from '../home/home'

export default function Islogin() {
    if (sessionStorage.getItem('istype')) {
        return (
            <Redirect path="/" to="/home"></Redirect>
        )
    } else {
        return (
            <Redirect path="/" to="/login"></Redirect>
        ) 
    }   
}