import './login.scss'
import { Input, Button, Form, Checkbox, Row, Col, Dropdown, Menu, Card } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import React from  'react';

class Aa extends React.Component {
    constructor(props) {
        super(props);
    }
    formRef = React.createRef();
    handleSave = async () => {
        // 通过表单实例触发表单验证
          // 如果校验不通过，将不会继续执行下面的代码；如果校验通过，可获取到表单域的值
        const values = await this.formRef.current.validateFields();
        sessionStorage.setItem("istype", true);
        this.props.history.push('/home');
    }
    state = {
        visible: false,
    };
    
    onFinish = values => {
        console.log(values);
    }
    onChange = e => {
        console.log(e.target.checked)
    }
    handleMenuClick = e => {
        console.log(e.key)
        // if (e.key === '3') {
        //     this.setState({ visible: false });
        // }
    }
    handleVisibleChange = flag => {
        this.setState({ visible: flag });
    }
    
    render() {
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        const menu = (
            <Menu onClick={this.handleMenuClick}>
                <Menu.Item key={ '#fff'} onClick={ this.onItemCl} >简约白</Menu.Item>
                <Menu.Item key={ '#red'}  onClick={ this.onItemCl} >中国红</Menu.Item>
                <Menu.Item key={ '#999'}  onClick={ this.onItemCl} >地狱黑</Menu.Item>
            </Menu>
          );
        return (
            <div className='login-container'>
                <div className="login-box">
                    <Form horizontal="true"   ref={this.formRef} onFinish={this.onFinish}>
                        <h1 className="title">React Ant Design</h1>
                        <Form.Item
                            {...formItemLayout}
                            label="账号"
                            name="username"
                            hasFeedback
                            rules={[ { required: true, message: '请输入账号' }]}>
                            <Input placeholder="请输入账户" size="large" ></Input>
                        </Form.Item>
                        <Form.Item
                            label="密码"
                            name="password"
                            size="large"
                            hasFeedback
                            {...formItemLayout}
                            rules={[{ required: true, whitespace: true, message: '请输密码' }]}>
                            <Input.Password type="password" placeholder="请输入密码" size="large"></Input.Password >
                        </Form.Item>
                        <Row>
                            <Col span={7} offset={14} >
                            <Checkbox  defaultChecked onChange={this.onChange} >记住我</Checkbox>
                                <Button type="primary" size="large" onClick={this.handleSave}>登录</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={6} offset={15} type="flex" align="middle" className="signs" style={{marginTop:20}}>
                                <span >注册</span>
                               <span style={{marginLeft:10}}>登录</span>
                            </Col>
                        </Row>

                        <Dropdown
                            overlay={menu}
                            onVisibleChange={this.handleVisibleChange}
                            visible={this.state.visible}
                            className="topics"
                        >
                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>Selected topics<DownOutlined />
                            </a>
                        </Dropdown>
                    </Form>
                </div>
            </div>
        )
    }
}
export default Aa