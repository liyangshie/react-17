
const defaultState = {
    inputValue: '123',
    list: [1, 2],
    val:'异步初始化'
};
export default (state = defaultState, action) => {
    console.log(state,action)
    switch (action.type) {
        case "LOGIN_CESHI":
            const newState = JSON.parse(JSON.stringify(state));
            newState.inputValue = action.payload;
            return newState
        case "JIAZAI_ZHONG":
            const newState1 = JSON.parse(JSON.stringify(state));
            newState1.val = "加载中";
            return  newState1
        case "ERROR":
            const newState2 = JSON.parse(JSON.stringify(state));
            newState2.val = '错误error';
            return newState2
        case "RESULT":
            const newState3 = JSON.parse(JSON.stringify(state));
            newState3.val = '成功result';
            return  newState3
        default:
            return state
    }
}