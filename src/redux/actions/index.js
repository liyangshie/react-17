import store from "..";

const LOGIN_CESHI = 'LOGIN_CESHI';
const LOGIN_CESHI2 = 'LOGIN_CESHI2';
const LOGIN_CESHI3 = 'LOGIN_CESHI3';


let onAction =  function(action) {
    switch (action.type) {
        case 'LOGIN_CESHI':
            return {
                type: LOGIN_CESHI,
                payload: action.payload
            };
        case 'LOGIN_CESHI2':
            return {
                type: LOGIN_CESHI2,
                payload: action.payload
            };
        case 'LOGIN_CESHI3':
            return {
                type: LOGIN_CESHI3,
                payload: action.payload
            };
        default:
            return action
    }
}
export default onAction