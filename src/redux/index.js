/* 
    Redux使用
    1:安装引入 redux  中间件 redux-thunk  redux-Promise
    2:Action定义修改某个store的val对象方法 store.dispatch()是View发出Action的唯一方法
    3:Reducer 是Action请求后用Reducer接收store Reducer必须给出一个新的state  store.subscribe刷新state View才会发生变化 
    4:异步操作 Action Reducer 中间加了阻端函数 阻端函数返回状态  reducer给出相应呈现,
*/
import { createStore , applyMiddleware} from 'redux'
import thunk from 'redux-thunk'

import reducer  from './reducer'

let enhancer = applyMiddleware(thunk);
const store = createStore(reducer, enhancer);
export default store
