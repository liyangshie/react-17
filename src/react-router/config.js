import Login from '../pages/login/login'
import Logout from '../pages/login/logout'
import Home from '../pages/home/home'
import User from '../pages/user/user'
import Users from '../components/Nopage'
import child from '../pages/home/children/index'
//同vue一样添加嵌套子路由,关键字children改成routes,设置跳转,传参:vue /this.router,react /this.props.routes
const router = [
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/home",
        component: Home,
        routes: [...child],
    },
    {
        path: "/user",
        component: User,
    },
    {
        path: "/users",
        component: Users,
    },
]
export default router