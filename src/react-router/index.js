/*
    React-Router使用
    知识点:1静态路由2嵌套路由3重定向4动态路由5权限管理
    路由简介:Router Switch Route Link Redirect
    react 和vue 思路一样,写法稍有区别 子路由都是集成到router.js 进行渲染 ,vue是占位符一个搞定  react是实列 Route接收到数据进行渲染,Switch 是精准匹配路由 , Redirect 不符合上述Switch中Route 在重定向
    动态路由 和vue 一样 

*/
import { Provider } from 'react-redux';
import './index.scss'
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect 
}  from"react-router-dom";

import routes from './config';
import Islogin from '../pages/login/isLogin'
import store from '../redux';
// import Child from '../pages/home/children/child1'


class app extends React.Component {
    render() {
        return (
            <Provider store={ store }>
                <Router>
                    <div className="main-container">
                        <Switch>
                            {routes.map((route, i) => {
                                if (route.exact) {
                                    return <Route key={ i } path= {route.path} exact
                                            render={props => (
                                                <route.component {...props} routes={route.routes} ></route.component>
                                            )}
                                            ></Route>
                                } else {
                                    return <Route  key={ i } path= {route.path}
                                        render={props => (
                                        <route.component {...props} routes={route.routes} ></route.component>
                                    )}
                                    ></Route>
                                }
                            })}
                            <Islogin></Islogin>
                        </Switch>
                    </div>
                </Router>
            </Provider>
        )
    }
}
export default app
